# MinimalFileUploadServer

## Simple webserver for file uploading

Reads server.conf file for port, endpoint, destination directory etc
Starts webserver at localhost:[server_port]/[server_endpoint]
Stores uploaded files to ./[upload_directory]