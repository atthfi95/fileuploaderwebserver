package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"text/template"
	"time"
)

var printDebug bool // True prints additional information to console, if logging true, logs information
var logEvents bool  // True writes upload events to server.log
var templates *template.Template

type Conf struct {
	Upload_page         string // Landing page at domain:port/endpoint
	Upload_directory    string // Directory where files are stored
	Uploader_endpoint   string //
	Downloader_endpoint string
	Server_port         int
	Max_upload_size     int
}

var configuration Conf

func Default_configuration() *Conf {
	Print("No server.conf detected, loading default Mvalues")
	configuration = Conf{Upload_page: "src/upload.html"}
	configuration.Upload_directory = "uploads"
	configuration.Uploader_endpoint = "/upload"
	configuration.Server_port = 8080
	configuration.Max_upload_size = 700
	Print(fmt.Sprintf("%+v", configuration))
	return &configuration
}

func Load_configuration() *Conf {
	buf, err := ioutil.ReadFile("./server.conf")
	if err != nil {
		Print("Failed to read server.conf\nFallback to default configuration!")
		return Default_configuration()
	}
	s := strings.Split(string(buf), "\n")
	Print(fmt.Sprintf("Loading ", len(s), " lines from server.conf\n"))
	for _, l := range s {
		if len(l) > 0 {
			if l[0] != '#' && l[0] != '[' {
				splice := strings.Split(l, "=")
				if splice[0] == "upload_page" {
					configuration.Upload_page = strings.Trim(splice[1], "\"")
				}
				if splice[0] == "upload_directory" {
					configuration.Upload_directory = strings.Trim(splice[1], "\"")
				}
				if splice[0] == "server_endpoint" {
					configuration.Uploader_endpoint = strings.Trim(splice[1], "\"")
				}
				if splice[0] == "server_port" {
					port, _ := strconv.Atoi(splice[1])
					configuration.Server_port = port
				}
				if splice[0] == "printDebug" && splice[1] == "true" {
					printDebug = true
				}
				if splice[0] == "logEvents" && splice[1] == "true" {
					logEvents = true
				}
			}
		}
	}
	return &configuration
}

// Print will log given messages to server.log if logEvents == true and print console if printDebug == true
func Print(str ...string) {
	if printDebug {
		fmt.Println(str)
	}
	if logEvents {
		Log(strings.Join(str, ","))
	}
}

// Log will write given messages to server.log
func Log(msg string) {
	if logEvents {
		f, err := os.OpenFile("server.log",
			os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			panic(err)
		}
		defer f.Close()
		t := time.Now().String()
		if _, err := f.WriteString(t + ": " + msg + "\n"); err != nil {
			panic(err)
		}
	}
	if printDebug {
		fmt.Println(msg)
	}
}

func display(w http.ResponseWriter, page string, data interface{}) {
	templates.ExecuteTemplate(w, page+".html", data)
}

func uploadFile(w http.ResponseWriter, r *http.Request) {
	// TODO: set max from configuration
	r.ParseMultipartForm(10 << 20)

	// Get handler for filename, size and headers
	file, handler, err := r.FormFile("myFile")
	if err != nil {
		fmt.Println("Error Retrieving the File")
		fmt.Println(err)
		return
	}

	defer Log(fmt.Sprintf("Uploaded File: %+v\n", handler.Filename, "size: %+v\n", handler.Size, "MIME: %+v\n", handler.Header))
	defer file.Close()

	// Create file
	dst, err := os.Create(configuration.Upload_directory + "/" + handler.Filename)
	defer dst.Close()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Copy the uploaded file to the created file on the filesystem
	if _, err := io.Copy(dst, file); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(w, "Successfully Uploaded File\n")
}

func uploadHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		display(w, "upload", nil)
	case "POST":
		uploadFile(w, r)
	}
}

func init() {
	// False by default, set via server.conf
	printDebug = false
	logEvents = false

	if _, err := os.Stat("server.conf"); os.IsNotExist(err) {
		Default_configuration()
	} else {
		Load_configuration()
	}

	templates = template.Must(template.ParseFiles(configuration.Upload_page))
	Print(fmt.Sprintf("Starting server at localhost:" + strconv.Itoa(configuration.Server_port) + configuration.Uploader_endpoint + "\n"))
}

func main() {
	start()
}

func start() {
	Print("Server running!")
	// UPLOADER ENDPOINT
	http.HandleFunc(configuration.Uploader_endpoint, uploadHandler)
	// Convert configuration port string to int
	http.ListenAndServe(":"+strconv.Itoa(configuration.Server_port), nil)
}
